//  Асинхронность - это когда часть кода вырывается из call-stack и переностися в режим ожидания до события,
//  по которому он становится в очередь на попадание в Event loop. Код в  "ожидании события" это обычно 
//  директивы типа setTimeout либо дтрективы обработки данных приходящих с удаленных ресурсов (типа Promise).

document.querySelector('.main').insertAdjacentHTML('beforeend', '<button class="button">Find You with IP</button>')
document.querySelector(".button").addEventListener("click", () =>  ipRequest());
class Place {
  constructor(cont, count, region, cit, distr) {
    this.cont = cont;
    this.count = count;
    this.region = (region === '') ? '  No Region in this place' : region;
    this.cit = (cit === '') ? '  No City in this place': cit;
    this.distr = (distr === '') ? '  No district in this place': distr;
  }
  render(container) {
    if (!!document.querySelector(".answer")){document.querySelector(".answer").remove()}
      container.insertAdjacentHTML(
        "beforeend",
        ` <div class="answer"><ul><li>Continent:    ${this.cont}</li> <li>Country:    ${this.count}</li><li>Region:    ${this.region}</li> 
      <li>City:    ${this.cit}</li> <li>District:    ${this.distr}</li></ul></div>` )}};
  const ipRequest = async () => {
    try { const { ip } = await fetch("https://api.ipify.org/?format=json").then((response) => response.json());
      const { continent, country, regionName, city, district } = await
        fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`).then((response) => response.json());
      new Place(continent, country, regionName, city, district).render(document.querySelector(".main"));} catch (error) {console.error('ERROR:', error)};
};  

